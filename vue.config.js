//const fs = require("fs");
const { version, name } = require("./package.json");

module.exports = {
  configureWebpack: {
    output: {
      filename: `js/${name}-${version}.min.js`,
    },
    optimization: {
      splitChunks: false,
    },
  },
  filenameHashing: false,
  css: {
    extract: false,
  },
  // devServer: {
  //   https: true,
  //   key: fs.readFileSync("./ignore/localhost-key.pem"),
  //   cert: fs.readFileSync("./ignore/localhost.pem"),
  // },
};
