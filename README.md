# gs-storefront-widgets

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Injecting into magento

1. Generate a localhost-compatible ssl certificate following [https://web.dev/how-to-use-local-https/](This guide)
2. Create an ignore folder in this project.
3. Place the key cert files there and point to them in vue.config.js like this:

```
devServer: {
  https: true,
  key: fs.readFileSync("./ignore/localhost-key.pem"),
  cert: fs.readFileSync("./ignore/localhost.pem"),
},
```

4. Go to Magento's Admin > Content > Footer and paste the following tag in the Miscellaneous HTML section:

```
<script type="text/javascript" src="https://localhost:8080/js/app.js"></script>
```

5. Go to System > Cache Management and Flush Magento Cache.
6. You're done. The script should load properly from now on

### General knowledge

Stages/ Interaction types

* Product Details page
* Home Page / Category Page (interaction with product lists)
* Mini Cart Removal
* Cart
* Checkout success

CUSTOMIZABLE OPTIONS 
 
By adding the next script on top of the plugin script, you will modify the plugin.
Modifying the RcCustomInterface will allow you to make some modifications. 

<script type="text/javascript">
  window.RcCustomInterface = {
    widget: {},
    pages: {}
  }
</script>

 

POSIBLE MODIFICATIONS
 
Default widget styles:
.subscription-dropdown: override this class to change the default subscription dropdown look and feel.
.subscription-radio-buttons: override this class to change the default radio button type look and feel.

WIDGET OBJECT 
* This section will allow you to modify the Labels of the current implementation

WIDGET PROPERTIES
  widget: {
    radioButtonLabels: {
          oneTimeLabel: One-time purchase,
          subscriptionTypeLabel: Subscribe,
    },
    checkoutCartLabels: {
        subscriptionTypeLabel: Subscription,
    },
  },


PAGES OBJECT 

This section will allow you to override the behaviour for the page you want that includes the HTML objects and Listeners. Current pages are: (productDetailPage, checkoutSuccessPage, checkoutCartPage, productListPage, miniCartPages). Each page object should have either the pathName or selector properties (both are used inside the isCurrentPage function to identify which page we are on).

PAGES PROPERTIES

productDetailPage {
  * productDetailPage: Inside this function, we use vanilla js to query for the options node and the productId and then we prepend the subscription radio button node along with the dropdown containing the selectable subscription options (there are also some hidden fields needed to submit the values).

  * handleSubmit: Here’s where you can modify the submit event for the add to cart button.Default behaviour listens to the from id: #product_addtocart_form

  * handleNodeEventListener: Here’s where you can modify the events that are fired from the node object. Default behaviour listens to the radio button by id: #subscriptionFormRadioButtons hiding or displaying it on change
}
checkoutSuccessPage {
  * handlePage: By default, this function clears the local storage.
}
checkoutCartPage {

  * handleItemRemoval: attaches an event listener per remove button and updates the local storage accordingly.

  * prepareCart: displays the subscription information for each product in the cart (product list is received from the local storage).
}
productListPage {
  * prepareSubscriptionItem: embeds the node responsible for displaying the subscription options inside the product card (this happens for every page that contains the following selector: li.product-item).
}
miniCartPages {
  * handleMiniCartItemRemoval: attaches observers to identify whenever a product is removed from the mini cart (also updates the local storage accordingly).
}

NOTES: The only exception to this rule is the miniCartPages since the mini cart is always visible no matter which page you are on. The handlePage function always receives the product list as the first parameter. All the pages can be overridden with custom functions if needed.

