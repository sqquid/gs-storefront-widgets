import { isEmpty } from "lodash";
import api from "../../utils/apiClient";

export const orderWebhook = () => {
  let orderId = document.querySelector(".checkout-success > p > span")?.innerText;

  if (isEmpty(orderId)) {
    orderId = document.querySelector(".order-number")?.innerText;
  }
  orderId = !isEmpty(orderId) ? orderId : "";
  api.orders.update.webhook({ orderId }).then((result) => {
    console.log(result);
  });
};
