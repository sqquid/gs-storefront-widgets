import {
  SUBSCRIPTION_TYPE_FORM_RADIO_BUTTONS,
  SUBSCRIPTION_TYPE_RADIO_BUTTON,
  SUBSCRIPTION_FREQUENCY_DROPDOWN,
  ONE_TIME_RADIO_BUTTON,
  ONE_TIME_PLAN,
  MOLLIE_UNIT_INPUT_PLUGIN,
  MOLLIE_FREQUENCY_INPUT_PLUGIN,
  MOLLIE_PURCHASE_INPUT_PLUGIN,
  PRE_PAID,
  DISCOUNT_AMOUNT_INPUT_PLUGIN,
  DISCOUNT_TYPE_INPUT_PLUGIN,
} from "../../model/constants.js";

import {
  buildSubscriptionTypeRadioButton,
  buildSubscriptionIntervalDropdown,
  buildMollieFrequencyInput,
  buildMollieUnitInput,
  buildMolliePurchaseInput,
  buildDiscountAmountInput,
  buildDiscountTypeInput,
} from "../../model/builders.js";

import { isEmpty } from "lodash";

import { showDiscount } from "../../utils/utils.js";

import api from "../../utils/apiClient";

export const getProductListCDN = async () => {
  let promises = [];
  const { productListItemClass } = window.RcInterface.widget.inputInsertClasses;
  const productList = document.querySelectorAll(productListItemClass);
  productList.forEach((product) => {
    const form = product.querySelector("[data-role=tocart-form]");
    const productId = form?.querySelector("[name=product]").value;
    if (productId) promises.push(api.recharge.products.get(productId));
  });
  const products = new Map();
  if (!isEmpty(promises)) {
    try {
      const result = await Promise.allSettled(promises);
      result.forEach((resultProduct) => {
        if (resultProduct.status === "fulfilled") {
          products.set(resultProduct.value.product.external_product_id, resultProduct.value.product);
        }
      });
    } catch (e) {
      console.log(e);
    }
  }
  return products;
};

export const prepareSubscriptionItem = (products) => {
  const { subscriptionInputClassSimpleProducts, subscriptionInputClassCustomProducts, productListItemClass } =
    window.RcInterface.widget.inputInsertClasses;

  const { showSubscriptionOptions } = window.RcInterface.pages.productListPage;
  if (!showSubscriptionOptions) return;

  const forms = document.querySelectorAll("[data-role=tocart-form]");
  if (isEmpty(forms)) return;
  const productList = document.querySelectorAll(productListItemClass);
  productList.forEach((product) => {
    const form = product.querySelector("[data-role=tocart-form]");
    const productId = form?.querySelector("[name=product]").value;
    const rechargeProduct = products?.get(productId);
    let hasOneTimePlan, nodoDetails;
    if (
      !isEmpty(rechargeProduct) &&
      !isEmpty(rechargeProduct.plans) &&
      isEmpty(rechargeProduct.plans.find((x) => x.type === PRE_PAID))
    ) {
      hasOneTimePlan = !isEmpty(rechargeProduct.plans.filter((plan) => plan.type === ONE_TIME_PLAN));
      nodoDetails = product.querySelector(subscriptionInputClassCustomProducts);
      if (!nodoDetails) {
        nodoDetails = product.querySelector(subscriptionInputClassSimpleProducts);
      }

      let div = document.createElement("div");
      div.innerHTML = `
      ${buildSubscriptionTypeRadioButton(productId, hasOneTimePlan)}
      ${buildSubscriptionIntervalDropdown(rechargeProduct, false, productId)}
    `;
      if (nodoDetails) {
        nodoDetails.append(div);

        let mollieDiv = document.createElement("div");
        mollieDiv.innerHTML = `
      ${buildMollieFrequencyInput(productId)}
      ${buildMollieUnitInput(productId)}
      ${buildMolliePurchaseInput(productId)}
      ${buildDiscountAmountInput(productId)}
      ${buildDiscountTypeInput(productId)}
    `;
        form.append(mollieDiv);

        document
          .querySelector(`#${SUBSCRIPTION_TYPE_FORM_RADIO_BUTTONS}${productId}`)
          .addEventListener("change", ({ target }) => {
            const { defaultValue } = target;
            let dropdown = document.querySelector(`#${SUBSCRIPTION_FREQUENCY_DROPDOWN}${productId}`);
            dropdown.style.display = defaultValue === SUBSCRIPTION_TYPE_RADIO_BUTTON ? "" : "none";
            const freq = dropdown.value.split(" ")[0];
            const unit = dropdown.value.split(" ")[1];
            const plan = rechargeProduct?.plans.find(
              (plan) => plan.order_interval_frequency == freq && plan.interval_unit === unit
            );
            const planPrice = rechargeProduct?.variants[0]?.prices[0]?.plans?.find(
              (planPrice) => planPrice.id === plan.id
            );
            showDiscount(planPrice, defaultValue, productId);
          });

        document.querySelector(`.swatch-opt-${productId}`)?.addEventListener("click", () => {
          let interValRef = 0;
          const checkState = () => {
            if (document.readyState == "complete") {
              clearInterval(interValRef);
              const subscriptionSelected = document.querySelector(
                `#${SUBSCRIPTION_TYPE_RADIO_BUTTON}${productId}`
              ).checked;
              const hasStrike =
                document.querySelector(`#product-price-${productId}`)?.querySelector("strike") ||
                document.querySelector(`#old-price-${productId}-widget-product-grid`)?.querySelector("strike");
              if (subscriptionSelected && !hasStrike) {
                let dropdown = document.querySelector(`#${SUBSCRIPTION_FREQUENCY_DROPDOWN}${productId}`);
                const freq = dropdown.value.split(" ")[0];
                const unit = dropdown.value.split(" ")[1];
                const plan = rechargeProduct?.plans.find(
                  (plan) => plan.order_interval_frequency == freq && plan.interval_unit === unit
                );
                const planPrice = rechargeProduct?.variants[0]?.prices[0]?.plans?.find(
                  (planPrice) => planPrice.id === plan.id
                );
                showDiscount(planPrice, SUBSCRIPTION_TYPE_RADIO_BUTTON, productId);
              }
            }
          };
          interValRef = setInterval(checkState, 10);
        });

        form.addEventListener("submit", ({ target: { elements } }) => {
          const radioButton = nodoDetails?.querySelector(`#${ONE_TIME_RADIO_BUTTON}${productId}`);
          const dropdown = nodoDetails?.querySelector(`#${SUBSCRIPTION_FREQUENCY_DROPDOWN}${productId}`);
          let mollieUnitInput = elements[MOLLIE_UNIT_INPUT_PLUGIN + productId];
          let mollieFrequencyInput = elements[MOLLIE_FREQUENCY_INPUT_PLUGIN + productId];
          let molliePurchaseInput = elements[MOLLIE_PURCHASE_INPUT_PLUGIN + productId];
          let discountTypeInput = elements[DISCOUNT_TYPE_INPUT_PLUGIN + productId];
          let discountAmountInput = elements[DISCOUNT_AMOUNT_INPUT_PLUGIN + productId];
          if ((!hasOneTimePlan || !radioButton?.checked) && !!dropdown) {
            const [frequency, unit] = dropdown?.value.split(" ");
            mollieUnitInput.value = unit;
            mollieFrequencyInput.value = frequency;
            molliePurchaseInput.value = SUBSCRIPTION_TYPE_RADIO_BUTTON;
            const plan = rechargeProduct?.plans.find(
              (plan) => plan.order_interval_frequency == frequency && plan.interval_unit === unit
            );
            discountTypeInput.value = plan.discount_type;
            discountAmountInput.value = plan.discount_amount;
          } else {
            if (!!mollieUnitInput && !!mollieFrequencyInput) {
              mollieUnitInput.value = "";
              mollieFrequencyInput.value = "";
              molliePurchaseInput.value = ONE_TIME_RADIO_BUTTON;
              discountTypeInput.value = "percentage";
              discountAmountInput.value = 0;
            }
          }
        });
      }
    }
  });
};
