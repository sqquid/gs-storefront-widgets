import { buildSubscriptionTypeRadioButton, buildSubscriptionIntervalDropdown } from "../../model/builders.js";

import { isEmpty } from "lodash";

import { showDiscount } from "../../utils/utils.js";

import {
  SUBSCRIPTION_TYPE_FORM_RADIO_BUTTONS,
  SUBSCRIPTION_TYPE_RADIO_BUTTON,
  SUBSCRIPTION_FREQUENCY_DROPDOWN,
  SUBSCRIPTION_RADIO_BUTTON_NAME,
  ONE_TIME_PLAN,
  MOLLIE_UNIT_INPUT_PLUGIN,
  MOLLIE_FREQUENCY_INPUT_PLUGIN,
  MOLLIE_PURCHASE_INPUT_PLUGIN,
  PRE_PAID,
  DISCOUNT_TYPE_INPUT_PLUGIN,
  DISCOUNT_AMOUNT_INPUT_PLUGIN,
} from "../../model/constants.js";

import api from "../../utils/apiClient";

export const getProductDetailCDN = async () => {
  const productId = document.querySelector(`[name="product"]`).value;
  const productCDN = await api.recharge.products.get(productId);
  return productCDN?.product;
};

export const handleSubscriptionRadioButtonEventListener = (productCDN) => {
  document.querySelector(`#${SUBSCRIPTION_TYPE_FORM_RADIO_BUTTONS}`)?.addEventListener("change", ({ target }) => {
    const { defaultValue } = target;
    let dropdown = document.querySelector(`#${SUBSCRIPTION_FREQUENCY_DROPDOWN}`);
    dropdown.style.display = defaultValue === SUBSCRIPTION_TYPE_RADIO_BUTTON ? "block" : "none";
    const productId = document.querySelector(`[name="product"]`).value;
    const freq = dropdown.value.split(" ")[0];
    const unit = dropdown.value.split(" ")[1];
    const plan = productCDN.plans.find((plan) => plan.order_interval_frequency == freq && plan.interval_unit === unit);
    const planPrice = productCDN.variants[0]?.prices[0]?.plans?.find((planPrice) => planPrice.id === plan.id);
    showDiscount(planPrice, defaultValue, productId);
  });
};

export const handleSubmitEventListener = (productCDN) => {
  document.querySelector(`#product_addtocart_form`).addEventListener("submit", ({ target: { elements } }) => {
    const hasOneTimePlan = !isEmpty(productCDN ? productCDN.plans.filter((plan) => plan.type === ONE_TIME_PLAN) : {});

    const radioButton = elements[SUBSCRIPTION_RADIO_BUTTON_NAME];
    const dropdown = elements[SUBSCRIPTION_FREQUENCY_DROPDOWN];
    let mollieUnitInput = elements[MOLLIE_UNIT_INPUT_PLUGIN];
    let mollieFrequencyInput = elements[MOLLIE_FREQUENCY_INPUT_PLUGIN];
    let molliePurchaseInput = elements[MOLLIE_PURCHASE_INPUT_PLUGIN];
    let discountTypeInput = elements[DISCOUNT_TYPE_INPUT_PLUGIN];
    let discountAmountInput = elements[DISCOUNT_AMOUNT_INPUT_PLUGIN];

    if ((!hasOneTimePlan || radioButton.value === SUBSCRIPTION_TYPE_RADIO_BUTTON) && !!dropdown) {
      const [frequency, unit] = dropdown.value.split(" ");
      if (!!mollieUnitInput && !!mollieFrequencyInput && !!molliePurchaseInput) {
        mollieUnitInput.value = unit;
        mollieFrequencyInput.value = frequency;
        molliePurchaseInput.value = "subscription";
        const plan = productCDN.plans.find(
          (plan) => plan.order_interval_frequency == frequency && plan.interval_unit === unit
        );
        discountTypeInput.value = plan.discount_type;
        discountAmountInput.value = plan.discount_amount;
      }
    } else {
      if (!!mollieUnitInput && !!mollieFrequencyInput && !!molliePurchaseInput) {
        mollieUnitInput.value = "";
        mollieFrequencyInput.value = "";
        molliePurchaseInput.value = "onetime";
        discountTypeInput.value = "percentage";
        discountAmountInput.value = 0;
      }
    }
  });
};

export const productDetailPage = (productCDN) => {
  const { showSubscriptionOptions } = window.RcInterface.pages.productDetailPage;
  if (!showSubscriptionOptions) return;
  let radioButtonNode = document.querySelector("#product_addtocart_form");

  const productId = document.querySelector("#product_addtocart_form").elements["product"].value;
  if (productCDN) {
    const hasOneTimePlan = !isEmpty(productCDN.plans.filter((plan) => plan.type === ONE_TIME_PLAN));
    const isPrePaidProduct = !isEmpty(productCDN.plans.find((x) => x.type === PRE_PAID));
    if (!isEmpty(productCDN) && !isEmpty(productCDN.plans) && !isPrePaidProduct) {
      let div = document.createElement("div");
      div.innerHTML = `
      ${buildSubscriptionTypeRadioButton(null, hasOneTimePlan)}
      ${buildSubscriptionIntervalDropdown(productCDN)}
    `;
      radioButtonNode.prepend(div);
      document.querySelector(`.swatch-opt`)?.addEventListener("click", () => {
        let interValRef = 0;
        const checkState = () => {
          if (document.readyState == "complete") {
            clearInterval(interValRef);
            const subscriptionSelected = document.querySelector(`#${SUBSCRIPTION_TYPE_RADIO_BUTTON}`).checked;
            const hasStrike = document.querySelector(`#product-price-${productId}`).querySelector("strike");
            if (subscriptionSelected && !hasStrike) {
              let dropdown = document.querySelector(`#${SUBSCRIPTION_FREQUENCY_DROPDOWN}`);
              const freq = dropdown.value.split(" ")[0];
              const unit = dropdown.value.split(" ")[1];
              const plan = productCDN.plans.find(
                (plan) => plan.order_interval_frequency == freq && plan.interval_unit === unit
              );
              const planPrice = productCDN.variants[0]?.prices[0]?.plans?.find((planPrice) => planPrice.id === plan.id);
              showDiscount(planPrice, SUBSCRIPTION_TYPE_RADIO_BUTTON, productId);
            }
          }
        };
        interValRef = setInterval(checkState, 10);
      });
    }
  }
};
