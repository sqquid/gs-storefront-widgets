import { isEmpty } from "lodash";
import {
  SUBSCRIPTION_TYPE_RADIO_BUTTON,
  SUBSCRIPTION_FREQUENCY_DROPDOWN,
  MOLLIE_PURCHASE_INPUT_PLUGIN,
  MOLLIE_UNIT_INPUT_PLUGIN,
  MOLLIE_FREQUENCY_INPUT_PLUGIN,
  DISCOUNT_TYPE_INPUT_PLUGIN,
  DISCOUNT_AMOUNT_INPUT_PLUGIN,
} from "../../model/constants.js";

export const handleProductUpdateSubscriptionDetails = () => {
  const { cart } = JSON.parse(localStorage.getItem("mage-cache-storage")) || {};

  const url = window.location.pathname.split("/");
  const idIndex = url.findIndex((item) => item === "id");
  const itemId = idIndex !== -1 ? url[idIndex + 1] : null;
  const product = !isEmpty(itemId) ? cart?.items?.find((item) => item.item_id === itemId) : {};
  const subscriptionOption = product.options?.find((option) => option.label === "Subscription");
  if (!isEmpty(subscriptionOption)) {
    const subscribeRadioButton = document.querySelector(`#${SUBSCRIPTION_TYPE_RADIO_BUTTON}`);
    const dropDown = document.querySelector(`#${SUBSCRIPTION_FREQUENCY_DROPDOWN}`);
    subscribeRadioButton.click();
    dropDown.value = subscriptionOption.value;
  }
};

export const handleProductUpdateListener = (product) => {
  document.querySelector(`#product-updatecart-button`).addEventListener("click", () => {
    const suscribeRadioButton = document.querySelector(`#${SUBSCRIPTION_TYPE_RADIO_BUTTON}`);

    let mollieUnitInput = document.querySelector(`#${MOLLIE_UNIT_INPUT_PLUGIN}`);
    let mollieFrequencyInput = document.querySelector(`#${MOLLIE_FREQUENCY_INPUT_PLUGIN}`);
    let molliePurchaseInput = document.querySelector(`#${MOLLIE_PURCHASE_INPUT_PLUGIN}`);
    let discountTypeInput = document.querySelector(`#${DISCOUNT_TYPE_INPUT_PLUGIN}`);
    let discountAmountInput = document.querySelector(`#${DISCOUNT_AMOUNT_INPUT_PLUGIN}`);

    molliePurchaseInput.value = "onetime";
    if (suscribeRadioButton?.checked) {
      const [frequency, unit] = document.querySelector(`#${SUBSCRIPTION_FREQUENCY_DROPDOWN}`).value.split(" ");
      mollieUnitInput.value = unit;
      mollieFrequencyInput.value = frequency;
      molliePurchaseInput.value = "subscription";
      const plan = product?.plans.find(
        (plan) => plan.order_interval_frequency == frequency && plan.interval_unit === unit
      );
      discountTypeInput.value = plan.discount_type;
      discountAmountInput.value = plan.discount_amount;
    } else {
      discountTypeInput.value = "percentage";
      discountAmountInput.value = 0;
    }
  });
};
