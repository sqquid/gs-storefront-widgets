export const handleWishlistNotification = () => {
  const modalHTML = `
  <div id="wishlist_modal" class="recharge-modal">
    <div class="recharge-modal-content">
      <header class="recharge-modal-header">
        <span class="recharge-modal-close">x</span>
      </header>
      <p>Adding this product to the wishlist will remove its subscription information</p>
      <footer class="recharge-modal-footer">
        <button class="recharge-modal-cancel">Cancel</button>
        <button class="recharge-modal-confirm">Confirm</button>
      </footer>
    </div>
  </div>
  `;

  document.body.innerHTML += modalHTML;

  const modal = document.getElementById("wishlist_modal");

  const btnClose = document.getElementsByClassName("recharge-modal-close")[0];
  btnClose.onclick = function () {
    modal.style.display = "none";
  };
  const btnCancel = document.getElementsByClassName("recharge-modal-cancel")[0];
  btnCancel.onclick = function () {
    modal.style.display = "none";
  };

  const productItems = document.querySelectorAll(".cart > .item");

  productItems?.forEach((productItem) => {
    const options = productItem.querySelectorAll(".item-options > dt");
    const hasSubscription = Array.from(options).find((option) => option.innerText === "Subscription");
    if (hasSubscription) {
      const wishlistLink = productItem.querySelector(".action-towishlist");
      wishlistLink?.addEventListener("click", function (event) {
        modal.style.display = "block";
        const btnConfirmation = document.getElementsByClassName("recharge-modal-confirm")[0];
        btnConfirmation.onclick = () => {
          wishlistLink.click();
          modal.style.display = "none";
        };
        if (event.isTrusted) {
          event.stopPropagation();
        }
      });
    }
  });
};
