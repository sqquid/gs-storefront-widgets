import Vue from "vue";
import App from "./App.vue";
import createStore from "./store";
import getInterface from "./interface/widgetInterface";

Vue.config.productionTip = false;
let store;
const getStore = () => {
  if (!store) {
    store = createStore();
  }
  return store;
};

const initiateVue = () => {
  const vueRoot = document.createElement("div");
  vueRoot.setAttribute("vue-root", "");
  document.body.append(vueRoot);
  getInterface()
    .then((widgetInterface) => {
      window.RcInterface = widgetInterface;
      window.RcVue = new Vue({
        el: vueRoot,
        store: getStore(),
        render: (h) => h(App),
      });
    })
    .catch((error) => {
      throw error;
    });
};

export default initiateVue;
