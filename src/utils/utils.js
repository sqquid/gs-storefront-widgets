import { SUBSCRIPTION_TYPE_RADIO_BUTTON } from "../model/constants.js";

export const showDiscount = (planPrice, defaultValue, productId) => {
  const hasOldPrice = document.querySelector(`[data-product-id="` + productId + `"]`).querySelector(".old-price");
  //If we have old price we should calculate the discount over it
  const priceSpan = hasOldPrice
    ? document
        .querySelector(`[data-product-id="` + productId + `"]`)
        .querySelector(".old-price")
        .querySelector(".price")
    : document.querySelector(`[data-product-id="` + productId + `"]`).querySelector(".price");
  if (defaultValue === SUBSCRIPTION_TYPE_RADIO_BUTTON) {
    const price = priceSpan.innerText;
    let priceNumber;
    let currency;
    if (isEuropeanStyle(price)) {
      priceNumber = Number(
        price
          .replace(/[^0-9.&,-]+/g, "")
          .replace(".", "")
          .replace(",", ".")
      );
      currency = price.substring(0, price.indexOf(price.replace(/[^0-9.&,-]+/g, "")));
    } else {
      priceNumber = Number(price.replace(/[^0-9.-]+/g, ""));
      currency = price.substring(0, price.indexOf(priceNumber));
    }
    const discountedPrice = Number(planPrice.discounted_price);
    if (discountedPrice != priceNumber) {
      const styleFormat = isEuropeanStyle(price) ? "es" : "en-US";
      const formatter = new Intl.NumberFormat(styleFormat, { minimumFractionDigits: 2 });
      const formattedPrice = currency + formatter.format(discountedPrice);
      if (hasOldPrice) {
        const finalPriceSpan = document.querySelector(`[data-product-id="` + productId + `"]`).querySelector(".price");
        finalPriceSpan.innerText = formattedPrice;
      } else {
        const strike = document.createElement("strike");
        strike.innerText = " (" + price + ")";
        priceSpan.innerText = formattedPrice;
        priceSpan.parentElement.append(strike);
      }
    }
  } else {
    const strike = priceSpan.parentElement.querySelector("strike");
    if (strike) {
      var regex = /\(([^)]+)\)/;
      var matches = regex.exec(strike.innerText);
      const originalPrice = matches[1];
      strike.remove();
      priceSpan.innerText = originalPrice;
    } else {
      const spanPreviousPrice = document
        .querySelector(`[data-product-id="` + productId + `"]`)
        .querySelector(".price-container.price-final_price");
      let oldPrice;
      spanPreviousPrice.childNodes.forEach((node) => {
        if (node.nodeType == 1 && node.hasAttribute("data-price-amount")) {
          oldPrice = Number(node.getAttribute("data-price-amount")).toFixed(2);
        }
      });
      const discountedPrice = Number(planPrice.discounted_price);
      if (discountedPrice != oldPrice) {
        const priceSpanModified = document
          .querySelector(`[data-product-id="` + productId + `"]`)
          .querySelector(".price");
        let currency;
        let format;
        if (isEuropeanStyle(priceSpanModified.innerText)) {
          currency = priceSpanModified.innerText.substring(
            0,
            priceSpanModified.innerText.indexOf(priceSpanModified.innerText.replace(/[^0-9.&,-]+/g, ""))
          );
          format = "es";
        } else {
          currency = priceSpanModified.innerText.substring(
            0,
            priceSpanModified.innerText.indexOf(Number(priceSpanModified.innerText.replace(/[^0-9.-]+/g, "")))
          );
          format = "en-US";
        }
        const formatter = new Intl.NumberFormat(format, { minimumFractionDigits: 2 });
        priceSpanModified.innerText = currency + formatter.format(oldPrice);
      }
    }
  }
};

const isEuropeanStyle = (price) => {
  const indexOfComma = price.indexOf(",");
  const indexOfDot = price.indexOf(".");
  return indexOfComma > indexOfDot;
};
