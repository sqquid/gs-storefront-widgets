import axios from "axios";
import { isEmpty } from "lodash";

let env = "local";
let apiUrl = null;
let customUrls = {};
let merchantId = null;

/**
 * configures the public and frontend-only api urls
 */
export const setupApiClient = (store, apiVersion = "v1", merchant, environment) => {
  if (isEmpty(apiUrl) || isEmpty(customUrls) || isEmpty(merchantId)) {
    customUrls = {
      cdnUrl: `https://static.rechargecdn.com/store/${store}/`,
    };
    merchantId = merchant;

    switch (environment) {
      case `dev1`:
        env = "dev1";
        apiUrl = `https://dev1-api.giantsqquid.com/${apiVersion}`;
        break;
      case `dev2`:
        env = "dev2";
        apiUrl = `https://dev2-api.giantsqquid.com/${apiVersion}`;
        break;
      case `dev3`:
        env = "dev3";
        apiUrl = `https://dev3-api.giantsqquid.com/${apiVersion}`;
        break;
      case `prod`:
        env = "prod";
        apiUrl = `https://api.giantsqquid.com/${apiVersion}`;
        break;
      default:
        env = "local";
        apiUrl = `http://localhost:4000/local/${apiVersion}`;
    }
  }
};

export const fetchData = ({ method = "post", endpoint, data = {}, customUrl = "", headers = {} }) => {
  const baseUrl = customUrls[customUrl] ? customUrls[customUrl] : apiUrl;

  let url = baseUrl + endpoint;

  const config = {
    env,
  };

  if (!isEmpty(headers)) {
    config.headers = Object.assign(config.headers, headers);
  }

  let request;

  if (method === "post" || method === "put" || method === "patch") {
    request = axios[method](url, data, config);
  } else {
    config.params = data;
    request = axios[method](url, config);
  }

  return request;
};

const _request = async (connection, request) => {
  const response = await connection(request);
  return response.data;
};

const recharge = () => ({
  products: {
    list: async (params) => {
      return _request(fetchData, {
        method: "get",
        endpoint: "product/2021-08/products.json",
        customUrl: "cdnUrl",
        data: params,
      });
    },
    get: async (id) => {
      return _request(fetchData, {
        method: "get",
        endpoint: `product/2021-08/${id}.json`,
        customUrl: "cdnUrl",
      });
    }
  },
  checkout: {
    redirect: async (params) => {
      return _request(fetchData, {
        method: "post",
        endpoint: `/merchants/${merchantId}/recharge/checkout`,
        data: params,
      });
    },
  },
});

const orders = () => ({
  update: {
    webhook: async (params) => {
      return _request(fetchData, {
        method: "post",
        endpoint: `/webhooks/${merchantId}/magento2/orders`,
        data: params,
      });
    },
  },
});

const customer = () => ({
  portal: {
    get: async ({ email }) => {
      return _request(fetchData, {
        method: "get",
        endpoint: `/merchants/${merchantId}/customer_portal/${email}`,
      });
    },
  },
});

const apiLibrary = {
  recharge: recharge(),
  orders: orders(),
  customer: customer(),
};

export default apiLibrary;
