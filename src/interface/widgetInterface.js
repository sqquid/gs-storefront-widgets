/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import * as Yup from "yup";
import _ from "lodash";
import {
  handleSubmitEventListener,
  handleSubscriptionRadioButtonEventListener,
  prepareSubscriptionItem,
  productDetailPage,
  getProductDetailCDN,
  getProductListCDN,
  handleProductUpdateSubscriptionDetails,
  handleProductUpdateListener,
  handleWishlistNotification,
  orderWebhook,
  addPortalLink,
} from "../components/pages";
import {
  ONE_TIME_RADIO_BUTTON_LABEL,
  SUBSCRIPTION_TYPE_RADIO_BUTTON_label,
  CHECKOUT_SUBSCRIPTION_TYPE_LABEL,
  SUBSCRIPTION_INPUT_CLASS_SIMPLE_LUMA_THEME,
  SUBSCRIPTION_INPUT_CLASS_CUSTOM_LUMA_THEME,
  PRODUCT_LIST_ITEM_CLASS_LUMA_THEME,
} from "../model/constants";

const defaultInterface = {
  widget: {
    radioButtonLabels: {
      oneTimeLabel: ONE_TIME_RADIO_BUTTON_LABEL,
      subscriptionTypeLabel: SUBSCRIPTION_TYPE_RADIO_BUTTON_label,
    },
    checkoutCartLabels: {
      subscriptionTypeLabel: CHECKOUT_SUBSCRIPTION_TYPE_LABEL,
    },
    inputInsertClasses: {
      subscriptionInputClassSimpleProducts: SUBSCRIPTION_INPUT_CLASS_SIMPLE_LUMA_THEME,
      subscriptionInputClassCustomProducts: SUBSCRIPTION_INPUT_CLASS_CUSTOM_LUMA_THEME,
      productListItemClass: PRODUCT_LIST_ITEM_CLASS_LUMA_THEME,
    },
  },
  pages: {
    checkoutCartPage: {
      selector: "",
      pathName: "checkout/cart/",
      isCurrentPage: function () {
        return (
          window.location.pathname.includes(this.pathName) || (this.selector && !!document.querySelector(this.selector))
        );
      },
      wishlistNotification: (...params) => {
        handleWishlistNotification(...params);
      },
      handlePage: function (...params) {
        this.wishlistNotification(...params);
      },
    },
    productDetailPage: {
      pathName: "",
      selector: ".product-info-main",
      showSubscriptionOptions: true,
      isCurrentPage: function () {
        return window.location.pathname === this.pathName || (this.selector && !!document.querySelector(this.selector));
      },
      getProductDetailCDN: (...params) => getProductDetailCDN(...params),
      handleSubmit: (...params) => handleSubmitEventListener(...params),
      handleNodeEventListener: (...params) => handleSubscriptionRadioButtonEventListener(...params),
      productDetailPage: (...params) => productDetailPage(...params),
      handlePage: function (...params) {
        this.getProductDetailCDN().then((result) => {
          this.productDetailPage(result, ...params);
          this.handleNodeEventListener(result, ...params);
          this.handleSubmit(result, ...params);
        });
      },
    },
    checkoutSuccessPage: {
      selector: "",
      pathName: "/checkout/onepage/success",
      isCurrentPage: function () {
        return (
          window.location.pathname.includes(this.pathName) || (this.selector && !!document.querySelector(this.selector))
        );
      },
      orderWebhook: () => {
        orderWebhook();
      },
      handlePage: function () {
        this.orderWebhook();
        localStorage.clear();
      },
    },
    productListPage: {
      selector: "li.product-item",
      pathName: "",
      showSubscriptionOptions: true,
      isCurrentPage: function () {
        return window.location.pathname === this.pathName || (this.selector && !!document.querySelector(this.selector));
      },
      prepareSubscriptionItem: (...params) => prepareSubscriptionItem(...params),
      getProductListCDN: (...params) => getProductListCDN(...params),
      handlePage: function (...params) {
        this.getProductListCDN().then((result) => this.prepareSubscriptionItem(result, ...params));
        
      },
    },
    productCartUpdatePage: {
      selector: "",
      pathName: "/checkout/cart/configure/id",
      isCurrentPage: function () {
        return (
          window.location.pathname.includes(this.pathName) || (this.selector && !!document.querySelector(this.selector))
        );
      },
      getProductDetailCDN: (...params) => getProductDetailCDN(...params),
      handleProductUpdateSubscriptionDetails: (...params) => handleProductUpdateSubscriptionDetails(...params),
      handleProductUpdateListener: (...params) => handleProductUpdateListener(...params),
      handlePage: function (...params) {
        this.getProductDetailCDN().then((result) => {
          this.handleProductUpdateSubscriptionDetails(result, ...params);
          this.handleProductUpdateListener(result, ...params);
        });
        
      },
    },
    myAccountPage: {
      selector: "",
      pathName: "/customer/account/",
      isCurrentPage: function () {
        return (
          window.location.pathname.includes(this.pathName) || (this.selector && !!document.querySelector(this.selector))
        );
      },
      addPortalLink: (...params) => addPortalLink(...params),
      handlePage: function (...params) {
        this.addPortalLink(...params);
      },
    },
  },
};

Yup.addMethod(Yup.object, "functionType", function (errorMessage) {
  return this.test(`function-type`, errorMessage, function (value) {
    const { path, createError } = this;

    return _.isFunction(value) || createError({ path, message: errorMessage });
  });
});

const Schema = Yup.object({
  pages: Yup.object({
    productDetailPage: Yup.object({
      selector: Yup.string(),
      pathName: Yup.string(),
      isCurrentPage: Yup.object().functionType("Must be a function"),
      productDetailPage: Yup.object().functionType("Must be a function"),
      handlePage: Yup.object().functionType("Must be a function"),
      handleSubmit: Yup.object().functionType("Must be a function"),
      handleNodeEventListener: Yup.object().functionType("Must be a function"),
      getProductDetailCDN: Yup.object().functionType("Must be a function"),
    }),
    checkoutSuccessPage: Yup.object({
      selector: Yup.string(),
      pathName: Yup.string(),
      isCurrentPage: Yup.object().functionType("Must be a function"),
      handlePage: Yup.object().functionType("Must be a function"),
    }),
    productListPage: Yup.object({
      selector: Yup.string(),
      pathName: Yup.string(),
      isCurrentPage: Yup.object().functionType("Must be a function"),
      prepareSubscriptionItem: Yup.object().functionType("Must be a function"),
      handlePage: Yup.object().functionType("Must be a function"),
      getProductListCDN: Yup.object().functionType("Must be a function"),
    }),
    myAccountPage: Yup.object({
      isCurrentPage: Yup.object().functionType("Must be a function"),
      addPortalLink: Yup.object().functionType("Must be a function"),
      handlePage: Yup.object().functionType("Must be a function"),
    }),
    productCartUpdatePage: Yup.object({
      isCurrentPage: Yup.object().functionType("Must be a function"),
      handleProductUpdateSubscriptionDetails: Yup.object().functionType("Must be a function"),
      handleProductUpdateListener: Yup.object().functionType("Must be a function"),
      handlePage: Yup.object().functionType("Must be a function"),
      getProductDetailCDN: Yup.object().functionType("Must be a function"),
    }),
  }),
});

const customInterface = !_.isNil(window.RcCustomInterface)
  ? _.merge(defaultInterface, window.RcCustomInterface || {})
  : defaultInterface;

const getInterface = async () => {
  await Schema.validate(customInterface);
  return customInterface;
};

export default getInterface;
