export const CDNResponseMock = {
  store_settings: {
    store_currency: {
      decimal_separator: null,
      zero_decimal_currency: false,
      currency_symbol: null,
      thousands_separator: null,
      currency_symbol_location: null,
      currency_code: "EUR",
    },
    external_store_hash: null,
    store_identifier: "gstest-hl",
    weight_unit: "grams",
    external_platform_domain: "headless-06472e5ea1eb4b02abe26955e1a547b0",
  },
  products: [
    {
      2094: {
        external_plan_group_id: null,
        variants: [
          {
            option_values: [],
            external_variant_id: "2094",
            prices: [
              {
                currency: "EUR",
                unit_price: "12.0000",
                compare_at_price: null,
                plans: [],
              },
            ],
          },
        ],
        plans: [],
        external_product_id: "2094",
      },
    },
    {
      7591: {
        external_product_id: "2096",
        external_plan_group_id: null,
        plans: [
          {
            id: 3229726,
            title: "Palm - Belgian Beer",
            type: "onetime",
            discount_amount: null,
            discount_type: null,
            external_plan_id: null,
            external_plan_name: null,
            sort_order: null,
            charge_interval_frequency: null,
            order_interval_frequency: null,
            interval_unit: null,
          },
          {
            id: 3229727,
            title: "Palm - Belgian Beer",
            type: "subscription",
            discount_amount: null,
            discount_type: "percentage",
            external_plan_id: null,
            external_plan_name: null,
            sort_order: null,
            charge_interval_frequency: 1,
            order_interval_frequency: 1,
            interval_unit: "month",
          },
          {
            id: 3229728,
            title: "Palm - Belgian Beer",
            type: "subscription",
            discount_amount: null,
            discount_type: "percentage",
            external_plan_id: null,
            external_plan_name: null,
            sort_order: null,
            charge_interval_frequency: 2,
            order_interval_frequency: 2,
            interval_unit: "month",
          },
        ],
        variants: [
          {
            external_variant_id: "2096",
            option_values: [],
            prices: [
              {
                currency: "EUR",
                unit_price: "14.0000",
                compare_at_price: null,
                plans: [
                  {
                    id: 3229726,
                    discounted_price: "14.00",
                    discount_value: "0.0000",
                  },
                  {
                    id: 3229727,
                    discounted_price: "14.00",
                    discount_value: "0.0000",
                  },
                  {
                    id: 3229728,
                    discounted_price: "14.00",
                    discount_value: "0.0000",
                  },
                ],
              },
            ],
          },
        ],
      },
    },
    {
      1562: {
        plans: [
          {
            external_plan_id: null,
            title: "Radiant Tee",
            id: 3229716,
            external_plan_name: null,
            charge_interval_frequency: null,
            order_interval_frequency: null,
            sort_order: null,
            type: "onetime",
            interval_unit: null,
            discount_type: null,
            discount_amount: null,
          },
          {
            discount_type: "percentage",
            interval_unit: "month",
            id: 3229717,
            external_plan_name: null,
            discount_amount: null,
            external_plan_id: null,
            charge_interval_frequency: 1,
            title: "Radiant Tee",
            type: "subscription",
            order_interval_frequency: 1,
            sort_order: null,
          },
          {
            discount_type: "percentage",
            id: 3229718,
            charge_interval_frequency: 2,
            interval_unit: "month",
            order_interval_frequency: 2,
            sort_order: null,
            external_plan_id: null,
            title: "Radiant Tee",
            external_plan_name: null,
            discount_amount: null,
            type: "subscription",
          },
        ],
        variants: [
          {
            prices: [
              {
                plans: [
                  {
                    id: 3229716,
                    discount_value: "0.0000",
                    discounted_price: "13.00",
                  },
                  {
                    discount_value: "0.0000",
                    discounted_price: "13.00",
                    id: 3229717,
                  },
                  {
                    id: 3229718,
                    discounted_price: "13.00",
                    discount_value: "0.0000",
                  },
                ],
                unit_price: "13.0000",
                currency: "EUR",
                compare_at_price: null,
              },
            ],
            option_values: [],
            external_variant_id: "1562",
          },
        ],
        external_plan_group_id: null,
        external_product_id: "1562",
      },
    },
  ],
};
