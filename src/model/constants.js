export const SUBSCRIPTION_TYPE_FORM_RADIO_BUTTONS = "subscriptionFormRadioButtons";
export const SUBSCRIPTION_TYPE_RADIO_BUTTON = "subscription";
export const SUBSCRIPTION_TYPE_RADIO_BUTTON_label = "Subscribe";
export const ONE_TIME_RADIO_BUTTON = "onetime";
export const ONE_TIME_RADIO_BUTTON_LABEL = "One-time purchase";
export const SUBSCRIPTION_FREQUENCY_DROPDOWN = "subscriptionFrequency";
export const SUBSCRIPTION_RADIO_BUTTON_NAME = "purchase";
export const MOLLIE_FREQUENCY_INPUT = "recurring_metadata[frequency]";
export const MOLLIE_UNIT_INPUT = "recurring_metadata[unit]";
export const MOLLIE_PURCHASE_INPUT = "purchase";
export const DISCOUNT_TYPE_INPUT = "discount_type";
export const DISCOUNT_AMOUNT_INPUT = "discount_amount";
export const CHECKOUT_SUBSCRIPTION_TYPE_LABEL = "Subscription";

//Product subscription plans
export const ONE_TIME_PLAN = "onetime";
export const SUBSCRIPTION_PLAN = "subscription";
export const PRE_PAID = "prepaid";

//PLUGIN CONSTANTS
export const MOLLIE_FREQUENCY_INPUT_PLUGIN = "subscription_frequency";
export const MOLLIE_UNIT_INPUT_PLUGIN = "subscription_unit";
export const MOLLIE_PURCHASE_INPUT_PLUGIN = "subscription_plan";
export const DISCOUNT_TYPE_INPUT_PLUGIN = "discount_type";
export const DISCOUNT_AMOUNT_INPUT_PLUGIN = "discount_amount";

//CLASSES TO INSERT HTML SUBSCRIPTION INPUTS
export const SUBSCRIPTION_INPUT_CLASS_SIMPLE_LUMA_THEME = ".price-box.price-final_price";
export const SUBSCRIPTION_INPUT_CLASS_CUSTOM_LUMA_THEME = ".price-box.price-final_price";
export const PRODUCT_LIST_ITEM_CLASS_LUMA_THEME = "li.product-item";
