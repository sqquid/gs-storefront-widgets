import { isEmpty } from "lodash";
import {
  SUBSCRIPTION_TYPE_FORM_RADIO_BUTTONS,
  SUBSCRIPTION_TYPE_RADIO_BUTTON,
  SUBSCRIPTION_RADIO_BUTTON_NAME,
  ONE_TIME_RADIO_BUTTON,
  SUBSCRIPTION_FREQUENCY_DROPDOWN,
  MOLLIE_FREQUENCY_INPUT,
  MOLLIE_UNIT_INPUT,
  MOLLIE_PURCHASE_INPUT,
  ONE_TIME_PLAN,
  SUBSCRIPTION_PLAN,
  MOLLIE_FREQUENCY_INPUT_PLUGIN,
  MOLLIE_UNIT_INPUT_PLUGIN,
  MOLLIE_PURCHASE_INPUT_PLUGIN,
  DISCOUNT_TYPE_INPUT,
  DISCOUNT_AMOUNT_INPUT,
  DISCOUNT_TYPE_INPUT_PLUGIN,
  DISCOUNT_AMOUNT_INPUT_PLUGIN,
} from "./constants.js";

export const buildSubscriptionTypeRadioButton = (productId = null, hasOneTimePlan = true) => {
  const { oneTimeLabel, subscriptionTypeLabel } = window.RcInterface.widget.radioButtonLabels;
  return `
    <div id="${SUBSCRIPTION_TYPE_FORM_RADIO_BUTTONS}${productId ?? ""}" class="subscription-radio-buttons" ${
    !hasOneTimePlan ? `style="display: none;` : ""
  } >
        <input id="${ONE_TIME_RADIO_BUTTON}${productId ?? ""}" type="radio" name="${SUBSCRIPTION_RADIO_BUTTON_NAME}${
    productId ?? ""
  }" value="${ONE_TIME_RADIO_BUTTON}"  ${hasOneTimePlan ? `checked` : ""}>
        <label for="${ONE_TIME_RADIO_BUTTON}${productId ?? ""}">${oneTimeLabel}</label><br>
        <input id="${SUBSCRIPTION_TYPE_RADIO_BUTTON}${
    productId ?? ""
  }" type="radio" name="${SUBSCRIPTION_RADIO_BUTTON_NAME}${
    productId ?? ""
  }" value="${SUBSCRIPTION_TYPE_RADIO_BUTTON}" ${!hasOneTimePlan ? `checked` : ""}>
        <label for="${SUBSCRIPTION_TYPE_RADIO_BUTTON}${productId ?? ""}">${subscriptionTypeLabel}</label><br><br>
    </div>`;
};

export const buildSubscriptionIntervalDropdown = (product, isForm = true, productId) => {
  const hasOneTimePlan = !isEmpty(product?.plans.filter((plan) => plan.type === ONE_TIME_PLAN));
  const plans = product?.plans.filter((plan) => plan.type === SUBSCRIPTION_PLAN);
  let options = "";
  plans.sort((plan1, plan2) => plan1.order_interval_frequency - plan2.order_interval_frequency)
  .forEach(({ order_interval_frequency, interval_unit }) => {
    options = options.concat(
      `<option value="${order_interval_frequency} ${interval_unit}" >${
        isForm ? "Subscription every" : "Every"
      } ${order_interval_frequency} ${interval_unit}${order_interval_frequency > 1 ? "s" : ""}</option>`
    );
  });
  return `<select id="${SUBSCRIPTION_FREQUENCY_DROPDOWN}${productId ?? ""}" ${
    hasOneTimePlan ? `style="display: none;"` : ""
  }  class="subscription-dropdown ${isForm ? "" : "product-item-dropdown"}">${options}</select>`;
};

export const buildMollieFrequencyInput = (productId) => {
  return `<input type="text" name="${MOLLIE_FREQUENCY_INPUT}" id="${MOLLIE_FREQUENCY_INPUT_PLUGIN}${
    productId ?? ""
  }" style="display: none;"></input>`;
};

export const buildMollieUnitInput = (productId) => {
  return `<input type="text" name="${MOLLIE_UNIT_INPUT}" id="${MOLLIE_UNIT_INPUT_PLUGIN}${
    productId ?? ""
  }" style="display: none;"></input>`;
};

export const buildMolliePurchaseInput = (productId) => {
  return `<input type="text" name="${MOLLIE_PURCHASE_INPUT}" id="${MOLLIE_PURCHASE_INPUT_PLUGIN}${
    productId ?? ""
  }" style="display: none;"></input>`;
};

export const buildDiscountAmountInput = (productId) => {
  return `<input type="text" name="${DISCOUNT_AMOUNT_INPUT}" id="${DISCOUNT_AMOUNT_INPUT_PLUGIN}${
    productId ?? ""
  }" style="display: none;"></input>`;
};

export const buildDiscountTypeInput = (productId) => {
  return `<input type="text" name="${DISCOUNT_TYPE_INPUT}" id="${DISCOUNT_TYPE_INPUT_PLUGIN}${
    productId ?? ""
  }" style="display: none;"></input>`;
};
