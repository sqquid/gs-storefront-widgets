import Vue from "vue";
import Vuex from "vuex";

const vuexStoreConfig = {
  state: {},
  mutations: {},
  actions: {},
  modules: {},
};

const createStore = () => {
  Vue.use(Vuex);
  return new Vuex.Store(vuexStoreConfig);
};

export default createStore;
